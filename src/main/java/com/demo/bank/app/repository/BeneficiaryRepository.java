package com.demo.bank.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.demo.bank.app.entity.Beneficiary;

public interface BeneficiaryRepository extends JpaRepository<Beneficiary, Integer> {

}
