package com.demo.bank.app.serviceimp;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.bank.app.dto.ApiResponse;
import com.demo.bank.app.dto.TransactionAmountDto;
import com.demo.bank.app.entity.Account;
import com.demo.bank.app.entity.Beneficiary;
import com.demo.bank.app.exception.userExist;
import com.demo.bank.app.repository.AccountRepository;
import com.demo.bank.app.repository.BeneficiaryRepository;
import com.demo.bank.app.service.TransactionService;

@Service
public class TransactionServiceImp implements TransactionService {

	
	private AccountRepository accountRepository;

	
	private BeneficiaryRepository beneficiaryRepository;
	@Autowired
	public TransactionServiceImp(AccountRepository accountRepository, BeneficiaryRepository beneficiaryRepository) {
		super();

		this.accountRepository = accountRepository;

		this.beneficiaryRepository = beneficiaryRepository;
	}

	@Override
	public ApiResponse adddtransactions(int accountid, int beneficiaryId, TransactionAmountDto transactionAmountDto) {

		Optional<Account> customeraccount = accountRepository.findById(accountid);
		Optional<Beneficiary> beneficiary = beneficiaryRepository.findById(beneficiaryId);

		if (beneficiary.isPresent()) {
			Beneficiary beneficiary1 = beneficiary.get();
			int beneficiaryAccountId = beneficiary1.getBeneficiaryAccountId();
			Optional<Account> benaccount = accountRepository.findById(beneficiaryAccountId);
			Account cusAccount = customeraccount.get();
			Account benfaccount = benaccount.get();

			int cusAmount = cusAccount.getAccountBal();
			int benAmount = benfaccount.getAccountBal();
			int mainamount = transactionAmountDto.getAmmount();
			if (cusAmount > mainamount) {
				cusAccount.setAccountBal(cusAmount - mainamount);
				benfaccount.setAccountBal(benAmount + mainamount);
				accountRepository.save(cusAccount);
				accountRepository.save(benfaccount);

			} else {
				throw new userExist("Insufficient Customer Balance");
			}
		} else {
			throw new userExist("benificiary not exist");
		}
		ApiResponse api = new ApiResponse();
		api.setStatus(200l);
		api.setMessage("Transaction was successful");
		return api;
	}

}
