package com.demo.bank.app.serviceimp;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.demo.bank.app.dto.ApiResponse;
import com.demo.bank.app.dto.TransactionAmountDto;
import com.demo.bank.app.entity.Account;
import com.demo.bank.app.entity.Beneficiary;
import com.demo.bank.app.entity.Customer;
import com.demo.bank.app.exception.userExist;
import com.demo.bank.app.repository.AccountRepository;
import com.demo.bank.app.repository.BeneficiaryRepository;
import com.demo.bank.app.service.TransactionService;

@ExtendWith(SpringExtension.class)
class TransactionServiceImpTest {

	@Mock
	private AccountRepository accountRepository;

	@Mock
	private BeneficiaryRepository beneficiaryRepository;
	private TransactionService transactionService;

	Customer customer;
	Account account;
	Account account1;
	Beneficiary beneficiary;
	TransactionAmountDto transactionAmountDto;

	@BeforeEach
	void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		transactionService = new TransactionServiceImp(accountRepository, beneficiaryRepository);
		customer = Customer.builder().adharNo(657456l).age(76).customerId(1).email("yamini@gmail.com").Login(true)
				.mobileNumber(6564l).name("yamini").password("hjgyuy").build();
		account = Account.builder().accountBal(76).accountId(1).accountNO("Acc565").accType("savings").branch("vizag")
				.customer(customer).build();
		beneficiary = Beneficiary.builder().beneficiaryAccountId(1).beneficiaryAccountNum("Acc565").beneficiaryId(2)
				.beneficiaryIFSC("kjhtyu").customeraccount(account).build();
		transactionAmountDto = TransactionAmountDto.builder().Ammount(0).build();
	}

	@Test
	void testAdddtransactions() {
		when(accountRepository.findById(account.getAccountId())).thenReturn(Optional.of(account));
		when(beneficiaryRepository.findById(beneficiary.getBeneficiaryId())).thenReturn(Optional.of(beneficiary));
		ApiResponse response = transactionService.adddtransactions(1, 2, transactionAmountDto);
		assertEquals(200L, response.getStatus());
		assertEquals("Transaction was successful", response.getMessage());
	}

	@Test
	void testAdddtransactions_beneficiaryNotExist() {
		when(accountRepository.findById(account.getAccountId())).thenReturn(Optional.of(account));
		when(beneficiaryRepository.findById(beneficiary.getBeneficiaryId())).thenReturn(Optional.empty());
		assertThrows(userExist.class, () -> transactionService.adddtransactions(1, 2, transactionAmountDto));

		try {
			transactionService.adddtransactions(1, 2, transactionAmountDto);
		} catch (userExist e) {
			assertEquals("benificiary not exist", e.getMessage());
		}
	}

}
