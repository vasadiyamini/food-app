package com.demo.bank.app.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import com.demo.bank.app.dto.AccountDto;
import com.demo.bank.app.dto.ApiResponse;

import com.demo.bank.app.service.AccountService;



@RestController
@RequestMapping("/account")
public class AccountController {


	private final AccountService accountService ;
	@Autowired
	AccountController(AccountService accountService){
		this.accountService = accountService;
	}
	
	@PutMapping("/{accountid}")
	public ResponseEntity<ApiResponse> updateProfile(@PathVariable int accountid,@RequestBody AccountDto accountdto ) {
		return  ResponseEntity.status(HttpStatus.OK).body(accountService.updateProfile(accountid, accountdto));
		
	}
	
}
