package com.demo.bank.app.serviceimp;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.bank.app.dto.ApiResponse;
import com.demo.bank.app.dto.BeneficiaryDto;
import com.demo.bank.app.entity.Account;
import com.demo.bank.app.entity.Beneficiary;
import com.demo.bank.app.entity.Customer;
import com.demo.bank.app.exception.userExist;
import com.demo.bank.app.repository.AccountRepository;
import com.demo.bank.app.repository.BeneficiaryRepository;
import com.demo.bank.app.repository.CustomerRepository;
import com.demo.bank.app.service.BeneficiaryService;
import java.util.Objects;

@Service
public class BeneficiaryServiceImp implements BeneficiaryService {
	private final CustomerRepository customerRepository;
	private final AccountRepository accountRepository;
	private final BeneficiaryRepository beneficiaryRepository;
	
	@Autowired
	public BeneficiaryServiceImp(CustomerRepository customerRepository, AccountRepository accountRepository,
			BeneficiaryRepository beneficiaryRepository) {
		super();
		this.customerRepository = customerRepository;
		this.accountRepository = accountRepository;
		this.beneficiaryRepository = beneficiaryRepository;
	}

	@Override
	public ApiResponse addbeneficiary(int accountid, BeneficiaryDto beneficiaryDto) {

		Optional<Account> account = accountRepository.findById(accountid);
		if (account.isEmpty()) {
			throw new userExist("Customer is not exist for this paticular Account id");
		}
		Optional<Customer> customer = customerRepository.findById(accountid);
		Customer customer1 = customer.get();
		Account account1 = account.get();

		boolean status = customer1.isLogin();
		if (status) {
			Optional<Account> benAccount = accountRepository.findById(beneficiaryDto.getBeneficiaryAccountId());
			Account benAccount1 = benAccount.get();
			if (benAccount.isEmpty()) {
				throw new userExist("There is no account exist for this beneficiary");
			} else if ((Objects.equals(benAccount1.getAccountNO(), beneficiaryDto.getBeneficiaryAccountNum()))
					&& (Objects.equals(benAccount1.getIfscCode(), beneficiaryDto.getBeneficiaryIFSC()))) {

				Beneficiary beneficiary = new Beneficiary();
				beneficiary.setBeneficiaryAccountId(beneficiaryDto.getBeneficiaryAccountId());
				beneficiary.setBeneficiaryAccountNum(beneficiaryDto.getBeneficiaryAccountNum());
				beneficiary.setBeneficiaryIFSC(beneficiaryDto.getBeneficiaryIFSC());

				beneficiary.setCustomeraccount(account1);
				beneficiaryRepository.save(beneficiary);
			} else {
				throw new userExist("Please correct with beneficiary account number or IFSC Code");
			}
		} else {
			throw new userExist("Customer should be login by using customer id and password");
		}

		ApiResponse api = new ApiResponse();
		api.setStatus(200l);
		api.setMessage("Beneficiary successfully added");
		return api;

	}
}
