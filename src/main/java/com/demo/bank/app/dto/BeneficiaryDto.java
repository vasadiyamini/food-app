package com.demo.bank.app.dto;



import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BeneficiaryDto {
	
	private String beneficiaryAccountNum;
	private int beneficiaryAccountId;
	private String beneficiaryIFSC;
	
}
