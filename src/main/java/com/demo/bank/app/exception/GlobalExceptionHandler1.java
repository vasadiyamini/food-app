package com.demo.bank.app.exception;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


@RestControllerAdvice
public class GlobalExceptionHandler1 extends ResponseEntityExceptionHandler {
	 @Override
		protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
				HttpHeaders headers, HttpStatusCode status, WebRequest request) {
			Map<String, String> errors = new HashMap<>();
			ex.getBindingResult().getAllErrors().forEach(error -> {
				String fieldName = ((FieldError) error).getField();
				String message = error.getDefaultMessage();
				errors.put(fieldName, message);
			});
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errors);
		}
	 
		@ExceptionHandler(BankingAppGlobalException1.class)
		protected ResponseEntity<ErrorResponse1> handleGlobalException(
				BankingAppGlobalException1 bookMyMovieGlobalException, Locale locale) {
			return ResponseEntity.badRequest().body(ErrorResponse1.builder().code(bookMyMovieGlobalException.getHttpStatus().value())
					.message(bookMyMovieGlobalException.getMessage()).build());
		}
	 
//		@ExceptionHandler(DataIntegrityViolationException.class)
//		public ResponseEntity<ErrorResponse1> handleDataIntegrityViolation(DataIntegrityViolationException ex) {
//	 
//			return ResponseEntity.badRequest().body(
//					ErrorResponse1.builder().code(HttpStatus.ALREADY_REPORTED).message("Resource already exist").build());
//		}
}
