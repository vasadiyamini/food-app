package com.demo.bank.app.serviceimp;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.bank.app.dto.AccountDto;
import com.demo.bank.app.dto.ApiResponse;
import com.demo.bank.app.entity.Account;
import com.demo.bank.app.entity.Customer;
import com.demo.bank.app.exception.userExist;
import com.demo.bank.app.repository.AccountRepository;
import com.demo.bank.app.repository.CustomerRepository;
import com.demo.bank.app.service.AccountService;

@Service
public class AccountServiceImp implements AccountService {
	
	
	private CustomerRepository customerRepository;
	private AccountRepository accountRepository;



	public AccountServiceImp() {
		super();
	}
	
	@Autowired
	public AccountServiceImp(CustomerRepository customerRepository, AccountRepository accountRepository) {
		super();
		this.customerRepository = customerRepository;
		this.accountRepository = accountRepository;
	}

	@Override
	public ApiResponse updateProfile(int accountid, AccountDto accountdto) {

		Optional<Account> account = accountRepository.findById(accountid);
		Optional<Customer> customer = customerRepository.findById(accountid);
		if (account.isEmpty()) {
			throw new userExist("Customer is not exist for this paticular Account id");
		}

		Customer customer1 = customer.get();
		if (account.isPresent()) {
			boolean status = customer1.isLogin();
			if (status) {
				Account account1 = account.get();
				account1.setAccType(accountdto.getAccType());
				account1.setBranch(accountdto.getBranch());
				int amount = account1.getAccountBal();
				account1.setAccountBal(accountdto.getAccountBal() + amount);
				if ("Bangalore".equals(accountdto.getBranch())) {
					account1.setIfscCode("BANG46128");
				} else if ("Chennai".equals(accountdto.getBranch())) {
					account1.setIfscCode("CHEN46128");
				} else {
					account1.setIfscCode("VIZAG46128");
				}

				accountRepository.save(account1);

			} else {
				throw new userExist("Customer should be login by using customer id and password");
			}
		}
		ApiResponse api = new ApiResponse();
		api.setStatus(200l);
		api.setMessage("Account successfully updated");
		return api;
	}
}
