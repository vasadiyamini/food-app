package com.demo.bank.app.controller;

import static org.junit.jupiter.api.Assertions.*;


import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import com.demo.bank.app.dto.ApiResponse;
import com.demo.bank.app.dto.CustomerDto;
import com.demo.bank.app.dto.LoginDto;
import com.demo.bank.app.service.CustomerService;


@ExtendWith(SpringExtension.class)
class CustomerControllerTest {
	@InjectMocks
private CustomerController customerController;
	
	@Mock
	CustomerService customerService;
	@Test
	void testAddRegistration() {
	CustomerDto customerDto = CustomerDto.builder().email("yamini@gmail.com").adharNo(3424l).age(19).mobileNumber(7675765675l).build();
	ApiResponse apiResponse = ApiResponse.builder().Status(201l).build();
	Mockito.when(customerService.addCustomer(customerDto)).thenReturn(apiResponse);
	ResponseEntity<ApiResponse> responseEntity=customerController.addRegistration(customerDto);
	assertEquals(201,responseEntity.getBody().getStatus());
	}

	@Test
	void testAddLogin() {
		LoginDto loginDto = LoginDto.builder().emailId("yamini").password("ghfghr").build();
		ApiResponse apiResponse = ApiResponse.builder().Status(201l).build();
		Mockito.when(customerService.addLogin(loginDto)).thenReturn(apiResponse);
		ResponseEntity<ApiResponse> responseEntity=customerController.addLogin(loginDto);
		assertEquals(201,responseEntity.getBody().getStatus());
	}

}
