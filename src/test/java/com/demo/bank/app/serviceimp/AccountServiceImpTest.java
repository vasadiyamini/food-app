package com.demo.bank.app.serviceimp;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.demo.bank.app.dto.AccountDto;
import com.demo.bank.app.dto.ApiResponse;

import com.demo.bank.app.entity.Account;
import com.demo.bank.app.entity.Customer;
import com.demo.bank.app.exception.userExist;
import com.demo.bank.app.repository.AccountRepository;
import com.demo.bank.app.repository.CustomerRepository;
import com.demo.bank.app.service.AccountService;

@ExtendWith(SpringExtension.class)
class AccountServiceImpTest {
	@Mock
	private CustomerRepository customerRepository;
	@Mock
	private AccountRepository accountRepository;

	private AccountService accountService;
	private Customer customer;
	private Account account;

	@BeforeEach
	void setUp() throws Exception {

		MockitoAnnotations.initMocks(this);
		accountService = new AccountServiceImp(customerRepository, accountRepository);
		customer = Customer.builder().customerId(1).email("yamini@gmail.com").password("yamini").Login(true).build();
		account = Account.builder().accountId(1).accountBal(1000).accountNO("Acc123").accType("Savings").branch("vizag")
				.customer(customer).build();

	}

	@Test
	void testUpdateProfile() {
		when(accountRepository.save(account)).thenReturn(account);

		AccountDto accountDto = AccountDto.builder().accountBal(1000).accType("Savings").branch("Savings").build();
		account.setAccountId(1);
		Optional<Account> optAccount = Optional.of(account);
		when(accountRepository.findById(1)).thenReturn(optAccount);
		when(customerRepository.findById(customer.getCustomerId())).thenReturn(Optional.of(customer));
		ApiResponse response = accountService.updateProfile(1, accountDto);
		assertEquals(200l, response.getStatus());
		assertEquals("Account successfully updated", response.getMessage());

	}

	@Test
	void testUpdateProfile_customerNotExsit() {
		when(accountRepository.save(account)).thenReturn(account);

		AccountDto accountDto = AccountDto.builder().accountBal(1000).accType("Savings").branch("Savings").build();
		account.setAccountId(1);
		when(accountRepository.findById(1)).thenReturn(Optional.empty());
		when(customerRepository.findById(customer.getCustomerId())).thenReturn(Optional.of(customer));
		assertThrows(userExist.class, () -> accountService.updateProfile(1, accountDto));
		try {
			accountService.updateProfile(1, accountDto);
		} catch (userExist e) {
			assertEquals("Customer is not exist for this paticular Account id", e.getMessage());
		}

	}

	@Test
	void testUpdateProfile_cusNotLogin() {
		when(accountRepository.save(account)).thenReturn(account);

		AccountDto accountDto = AccountDto.builder().accountBal(1000).accType("Savings").branch("Savings").build();

		account.setAccountId(1);
		customer.setLogin(false);
		Optional<Account> optAccount = Optional.of(account);
		when(accountRepository.findById(1)).thenReturn(optAccount);
		when(customerRepository.findById(customer.getCustomerId())).thenReturn(Optional.of(customer));
		assertThrows(userExist.class, () -> accountService.updateProfile(1, accountDto));
		try {
			accountService.updateProfile(1, accountDto);
		} catch (userExist e) {
			assertEquals("Customer should be login by using customer id and password", e.getMessage());
		}

	}
}