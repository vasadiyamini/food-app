package com.demo.bank.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.bank.app.dto.ApiResponse;
import com.demo.bank.app.dto.BeneficiaryDto;
import com.demo.bank.app.service.BeneficiaryService;


@RestController
@RequestMapping("/beneficiary")
public class BeneficiaryController {

	private final BeneficiaryService beneficiaryService;
	@Autowired
	BeneficiaryController(BeneficiaryService beneficiaryService){
		this.beneficiaryService = beneficiaryService;
	}

@PostMapping("/{accountid}")

public ResponseEntity<ApiResponse> adddbeneficiary(@PathVariable int accountid, @RequestBody BeneficiaryDto beneficiaryDto){
	return ResponseEntity.status(HttpStatus.CREATED).body(beneficiaryService.addbeneficiary(accountid,beneficiaryDto));
}
}
