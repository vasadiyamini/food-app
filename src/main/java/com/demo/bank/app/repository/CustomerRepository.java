package com.demo.bank.app.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.demo.bank.app.entity.Customer;
import java.util.List;



public interface CustomerRepository extends JpaRepository <Customer, Integer> {


	Optional<Customer> findByPassword(String password);
	
	Optional<Customer> findByEmail(String email);
	

	
}
