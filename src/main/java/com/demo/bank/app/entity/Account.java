package com.demo.bank.app.entity;





import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Account {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
private int accountId;
	
private String accountNO;
private String accType;
private String branch;
private String ifscCode;
private int accountBal; 
@OneToOne(cascade = CascadeType.ALL) 
@JoinColumn(name = "customerId")
private Customer customer;

}
