package com.demo.bank.app.service;

import com.demo.bank.app.dto.ApiResponse;
import com.demo.bank.app.dto.TransactionAmountDto;

public interface TransactionService {

	ApiResponse adddtransactions(int accountid, int beneficiaryId, TransactionAmountDto transactionAmountDto);

}
