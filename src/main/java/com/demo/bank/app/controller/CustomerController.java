package com.demo.bank.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.bank.app.dto.ApiResponse;
import com.demo.bank.app.dto.CustomerDto;
import com.demo.bank.app.dto.LoginDto;
import com.demo.bank.app.service.CustomerService;


import jakarta.validation.Valid;

@RestController
@RequestMapping("/customer")
public class CustomerController {
	private final CustomerService customerService;
	@Autowired
	CustomerController(CustomerService customerService){
		this.customerService = customerService;
	}


@PostMapping("/")

public ResponseEntity<ApiResponse> addRegistration(@Valid @RequestBody CustomerDto cusomerDto){
	return ResponseEntity.status(HttpStatus.CREATED).body(customerService.addCustomer(cusomerDto));
}	
	@PostMapping("/login")
	public ResponseEntity<ApiResponse> addLogin(@RequestBody LoginDto logindto) {
		return ResponseEntity.status(HttpStatus.CREATED).body(customerService.addLogin(logindto));
	

}
}