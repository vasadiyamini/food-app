package com.demo.bank.app.serviceimp;

import static org.assertj.core.api.Assertions.assertThat;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import com.demo.bank.app.dto.ApiResponse;
import com.demo.bank.app.dto.CustomerDto;
import com.demo.bank.app.dto.LoginDto;
import com.demo.bank.app.entity.Customer;
import com.demo.bank.app.exception.userExist;
import com.demo.bank.app.repository.AccountRepository;
import com.demo.bank.app.repository.CustomerRepository;
import com.demo.bank.app.service.CustomerService;

@ExtendWith(SpringExtension.class)
class CustomerServiceImpTest {
	@Mock
	private CustomerRepository customerRepository;
	@Mock
	private AccountRepository accountRepository;

	private CustomerService customerService;
	

	
	@BeforeEach
	void setUp() throws Exception {

		MockitoAnnotations.initMocks(this);
		customerService = new CustomerServiceImp(customerRepository, accountRepository);

	}

	@Test
	void testAddCustomer() {
		CustomerDto cusomerDto = CustomerDto.builder().email("yamini@gmail.com").age(12).name("yamini").build();	
		when(customerRepository.findByEmail(cusomerDto.getEmail())).thenReturn(Optional.empty());
		ApiResponse response = customerService.addCustomer(cusomerDto);

		assertEquals(200L, response.getStatus());
		assertEquals("Sucessfully customer added", response.getMessage());

	}

	@Test
	void testAddCustomer_AlredyExist() {
		CustomerDto cusomerDto = CustomerDto.builder().email("yamini@gmail.com").age(12).name("yamini").build();
		Customer customer = new Customer();
		when(customerRepository.findByEmail(cusomerDto.getEmail())).thenReturn(Optional.of(customer));
		assertThrows(userExist.class, () -> customerService.addCustomer(cusomerDto));
		try {
			customerService.addCustomer(cusomerDto);
		} catch (userExist e) {
			assertEquals("this user alredy register", e.getMessage());
		}
	}

	@Test
	void testAddLogin() {

		LoginDto logindto = LoginDto.builder().emailId("yamini@123").password("yamini").build();
		Customer cusomer = Customer.builder().email("yamini@gmail.com").age(12).name("yamini").Login(true)
				.password("yamini").build();
		when(customerRepository.findByEmail(logindto.getEmailId())).thenReturn(Optional.of(cusomer));
		//when(customerRepository.findByPassword(logindto.getPassword())).thenReturn(Optional.of(cusomer));
		ApiResponse response = customerService.addLogin(logindto);
		assertEquals(200l, response.getStatus());
		assertEquals("Customer Sucessfully Login", response.getMessage());
		assertThat(customerService.addLogin(logindto)).isEqualTo(response);
	}

	@Test
	void testAddLogin_CustomerNotExist() {

		LoginDto logindto = LoginDto.builder().emailId("yamini@123").password("yamini").build();
		Customer cusomer = Customer.builder().email("yamini@gmail.com").age(12).name("yamini").Login(true)
				.password("yamini").build();
		when(customerRepository.findByEmail(logindto.getEmailId())).thenReturn(Optional.empty());
		when(customerRepository.findByPassword(logindto.getPassword())).thenReturn(Optional.of(cusomer));
		assertThrows(userExist.class, () -> customerService.addLogin(logindto));
		try {
			customerService.addLogin(logindto);
		} catch (userExist e) {
			assertEquals("Customer not exits", e.getMessage());
		}
	}

	@Test
	void testAddLogin_IcorrectPassword() {

		LoginDto logindto = LoginDto.builder().emailId("yamini@123").password("yamini").build();
		Customer cusomer = Customer.builder().email("yamini@gmail.com").age(12).name("yamini").Login(true)
				.password("yami").build();
		when(customerRepository.findByEmail(logindto.getEmailId())).thenReturn(Optional.of(cusomer));
		//when(customerRepository.findByPassword(logindto.getPassword())).thenReturn(Optional.empty());
		assertThrows(userExist.class, () -> customerService.addLogin(logindto));
		try {
			customerService.addLogin(logindto);
		} catch (userExist e) {
			assertEquals("password is incorrect", e.getMessage());
		}

	}
}