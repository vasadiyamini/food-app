package com.demo.bank.app.dto;


import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CustomerDto {
	
	@NotNull(message = "name is requred field")
	private String name;
	@Min(value =18, message = "age must be at least 18")
	private int age;
	@NotNull(message = "mail is requred field")
	@Email(message = "invalid Email Id")
	private String email;
	@NotNull(message = "adhar is requred field")
	private Long adharNo;
	//@Size(min = 1, max = 12)
	private Long mobileNumber;
}
