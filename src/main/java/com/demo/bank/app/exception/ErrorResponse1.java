package com.demo.bank.app.exception;



import lombok.Builder;
import lombok.Data;
@Data
@Builder
public class ErrorResponse1 {
	
	private int code;
	private String message;
}
