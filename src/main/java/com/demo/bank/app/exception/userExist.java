package com.demo.bank.app.exception;



public class userExist extends BankingAppGlobalException1 {
	private static final long serialVersionUID = 1L;

	public userExist() {
		super("resource not found", GlobalErrorCode1.ERROR_RESOURCE_NOT_FOUND);
	}

	public userExist(String message) {
		super(message, GlobalErrorCode1.ERROR_RESOURCE_NOT_FOUND);
	}
	
}
