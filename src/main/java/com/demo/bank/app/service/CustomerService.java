package com.demo.bank.app.service;

import com.demo.bank.app.dto.ApiResponse;
import com.demo.bank.app.dto.CustomerDto;
import com.demo.bank.app.dto.LoginDto;

public interface CustomerService {
	
	ApiResponse addCustomer(CustomerDto cusomerDto);

	ApiResponse addLogin(LoginDto logindto);
}
