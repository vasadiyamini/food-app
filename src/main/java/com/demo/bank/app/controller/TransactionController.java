package com.demo.bank.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.demo.bank.app.dto.ApiResponse;
import com.demo.bank.app.dto.TransactionAmountDto;
import com.demo.bank.app.service.TransactionService;

@RestController
@RequestMapping("/transactions")
public class TransactionController {

	private final TransactionService transactionService;
	@Autowired
	TransactionController(TransactionService transactionService){
		this.transactionService = transactionService;
	}
	@PostMapping("/{accountid}/{beneficiaryId}")

	public ResponseEntity<ApiResponse> adddtransaction(@PathVariable int accountid, @PathVariable int beneficiaryId,
			@RequestBody TransactionAmountDto transactionAmountDto) {
		return ResponseEntity.status(HttpStatus.CREATED)
				.body(transactionService.adddtransactions(accountid, beneficiaryId, transactionAmountDto));
	}

}
