package com.demo.bank.app.entity;



import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class TransactionsAmount {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int transactionId;	
	private int accuntId;
	private int BenAccountId;
	private int Ammount;
}
