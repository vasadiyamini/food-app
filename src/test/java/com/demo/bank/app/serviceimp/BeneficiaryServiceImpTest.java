package com.demo.bank.app.serviceimp;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.demo.bank.app.dto.ApiResponse;
import com.demo.bank.app.dto.BeneficiaryDto;
import com.demo.bank.app.entity.Account;
import com.demo.bank.app.entity.Customer;
import com.demo.bank.app.exception.userExist;
import com.demo.bank.app.repository.AccountRepository;
import com.demo.bank.app.repository.BeneficiaryRepository;
import com.demo.bank.app.repository.CustomerRepository;
import com.demo.bank.app.service.BeneficiaryService;

@ExtendWith(SpringExtension.class)
class BeneficiaryServiceImpTest {
	@Mock
	private CustomerRepository customerRepository;
	@Mock
	private AccountRepository accountRepository;
	@Mock
	private BeneficiaryRepository beneficiaryRepository;
	private BeneficiaryService beneficiaryService;
	private Customer customer;
	private Account account;

	private BeneficiaryDto beneficiaryDto;

	@BeforeEach
	void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		beneficiaryService = new BeneficiaryServiceImp(customerRepository, accountRepository, beneficiaryRepository);
		customer = Customer.builder().adharNo(567648767l).age(22).customerId(1).email("yamini@gmail.com").Login(true)
				.mobileNumber(7876564l).name("yamini").password("hhvtyr").build();
		account = Account.builder().accountBal(75675).accountId(1).accountNO("678567").accType("Saving").branch("Vizag")
				.customer(customer).ifscCode("vizag461").build();

		beneficiaryDto = BeneficiaryDto.builder().beneficiaryAccountId(1).beneficiaryAccountNum("678567")
				.beneficiaryIFSC("vizag461").build();

	}

	@Test
	void testAddbeneficiary() {

		when(accountRepository.findById(customer.getCustomerId())).thenReturn(Optional.of(account));
		when(customerRepository.findById(customer.getCustomerId())).thenReturn(Optional.of(customer));
		when(accountRepository.findById(beneficiaryDto.getBeneficiaryAccountId())).thenReturn(Optional.of(account));
		ApiResponse response = beneficiaryService.addbeneficiary(1, beneficiaryDto);

		assertEquals(200, response.getStatus().intValue());
		assertEquals("Beneficiary successfully added", response.getMessage());
	}

	@Test
	void testAddbeneficiary_incorrectCredentials() {
		account.setIfscCode("hgytyr");
		when(accountRepository.findById(customer.getCustomerId())).thenReturn(Optional.of(account));
		when(customerRepository.findById(customer.getCustomerId())).thenReturn(Optional.of(customer));

		assertThrows(userExist.class, () -> beneficiaryService.addbeneficiary(1, beneficiaryDto));

		try {
			beneficiaryService.addbeneficiary(1, beneficiaryDto);
		} catch (userExist e) {
			assertEquals("Please correct with beneficiary account number or IFSC Code", e.getMessage());

		}
	}

	@Test
	void testAddbeneficiary_CustomerNotExist() {
		when(accountRepository.findById(customer.getCustomerId())).thenReturn(Optional.empty());
		when(customerRepository.findById(customer.getCustomerId())).thenReturn(Optional.empty());
		assertThrows(userExist.class, () -> beneficiaryService.addbeneficiary(1, beneficiaryDto));
		try {
			beneficiaryService.addbeneficiary(1, beneficiaryDto);
		} catch (userExist e) {
			assertEquals("Customer is not exist for this paticular Account id", e.getMessage());
		}
	}

	@Test
	void testAddbeneficiary_CustomerLogin() {
		customer.setLogin(false);
		when(accountRepository.findById(customer.getCustomerId())).thenReturn(Optional.of(account));
		when(customerRepository.findById(customer.getCustomerId())).thenReturn(Optional.of(customer));
		assertThrows(userExist.class, () -> beneficiaryService.addbeneficiary(1, beneficiaryDto));
		try {
			beneficiaryService.addbeneficiary(1, beneficiaryDto);
		} catch (userExist e) {
			assertEquals("Customer should be login by using customer id and password", e.getMessage());
		}
	}
//	@Test
//	 void testAddBeneficiary_AccountNotExist() {
//		account.setAccountId(7);
//		customer.setCustomerId(8);
//		beneficiaryDto.setBeneficiaryAccountId(5);
//		when(accountRepository.findById(customer.getCustomerId())).thenReturn(Optional.empty());
//		when(customerRepository.findById(customer.getCustomerId())).thenReturn(Optional.empty());
//		when(accountRepository.findById(beneficiaryDto.getBeneficiaryAccountId())).thenReturn(Optional.empty());
//		ApiResponse response = beneficiaryService.addbeneficiary(1, beneficiaryDto);
//		 
//	    assertEquals(200, response.getHttpStatus().intValue());
//	    assertEquals("Beneficiary successfully added", response.getMessage());
//}
}