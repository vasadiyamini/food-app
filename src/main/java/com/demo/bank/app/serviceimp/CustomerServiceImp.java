package com.demo.bank.app.serviceimp;

import java.util.Optional;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.bank.app.dto.ApiResponse;
import com.demo.bank.app.dto.CustomerDto;
import com.demo.bank.app.dto.LoginDto;
import com.demo.bank.app.entity.Account;
import com.demo.bank.app.entity.Customer;
import com.demo.bank.app.exception.userExist;
import com.demo.bank.app.repository.AccountRepository;
import com.demo.bank.app.repository.CustomerRepository;
import com.demo.bank.app.service.CustomerService;

@Service
public class CustomerServiceImp implements CustomerService {
	
	private final CustomerRepository customerRepository;	
	private final AccountRepository accountRepository;


	@Autowired
	public CustomerServiceImp(CustomerRepository customerRepository, AccountRepository accountRepository) {
		super();
		this.customerRepository = customerRepository;
		this.accountRepository = accountRepository;
	}

	@Override
	public ApiResponse addCustomer(CustomerDto cusomerDto) {
		Optional<Customer> byEmail = customerRepository.findByEmail(cusomerDto.getEmail());
		if (byEmail.isPresent()) {
			throw new userExist("this user alredy register");
		}
		String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789~`!@#$%^&*()";
		String pwd = RandomStringUtils.random(7, characters);

		Customer customer = Customer.builder().adharNo(cusomerDto.getAdharNo()).age(cusomerDto.getAge())
				.email(cusomerDto.getEmail()).name(cusomerDto.getName()).password(pwd)
				.mobileNumber(cusomerDto.getMobileNumber()).Login(false).build();
		customerRepository.save(customer);
		String characters1 = "0123456789";
		String pwd1 = RandomStringUtils.random(7, characters1);
		String pwd2 = "ACC" + pwd1;
		Account account = Account.builder().customer(customer).accountBal(10000).branch("  ").accType(" ")
				.accountNO(pwd2).build();
		accountRepository.save(account);

		ApiResponse api = new ApiResponse();
		api.setStatus(200l);
		api.setMessage("Sucessfully customer added");
		return api;
	}

	@Override
	public ApiResponse addLogin(LoginDto logindto) {
		
		Customer cus = customerRepository.findByEmail(logindto.getEmailId()).orElseThrow(()-> new userExist("Customer not exits"));
		
		if(logindto.getPassword().equals(cus.getPassword())) {
			cus.setLogin(true);
			customerRepository.save(cus);
		ApiResponse api = new ApiResponse();
		api.setStatus(200l);
		api.setMessage("Customer Sucessfully Login");
		
		return api;
		}
		else 
			throw new userExist("password is incorrect");	
	}
}
