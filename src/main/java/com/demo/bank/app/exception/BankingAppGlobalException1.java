package com.demo.bank.app.exception;

import org.springframework.http.HttpStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BankingAppGlobalException1 extends RuntimeException {
	
	private static final long serialVersionUID = 1L;
	private String message;
	private HttpStatus httpStatus;
	
	public BankingAppGlobalException1(String message) {
		super(message);
		
	}

	

	

	
}
