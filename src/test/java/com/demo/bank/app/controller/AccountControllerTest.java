package com.demo.bank.app.controller;




import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.demo.bank.app.dto.AccountDto;
import com.demo.bank.app.dto.ApiResponse;
import com.demo.bank.app.service.AccountService;
@ExtendWith(SpringExtension.class)
class AccountControllerTest {
	@InjectMocks
	private AccountController accountController;
	@Mock
	AccountService accountService; 
	@Test
	void testUpdateProfile() {
		
		AccountDto accountdto = AccountDto.builder().accountBal(89089).accType("Savings").branch("Vizag").build();
		ApiResponse apiResponse = ApiResponse.builder().Status(200l).build();
		Mockito.when(accountService.updateProfile(1, accountdto)).thenReturn(apiResponse);
		ResponseEntity<ApiResponse> responseEntity = accountController.updateProfile(1, accountdto);
		assertNotNull(responseEntity);
		assertEquals(200,responseEntity.getBody().getStatus());
	}
	@Test
void testUpdateProfile_Failure() {
		
		AccountDto accountdto = AccountDto.builder().accountBal(89089).accType("Savings").branch("Vizag").build();
		ApiResponse apiResponse = ApiResponse.builder().Status(200l).build();
		Mockito.when(accountService.updateProfile(1, accountdto)).thenReturn(apiResponse);
		ResponseEntity<ApiResponse> responseEntity = accountController.updateProfile(1, accountdto);
		//assertNotNull(responseEntity);
		assertEquals(200,responseEntity.getBody().getStatus());
	}

}
