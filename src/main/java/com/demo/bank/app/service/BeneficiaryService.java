package com.demo.bank.app.service;

import com.demo.bank.app.dto.ApiResponse;
import com.demo.bank.app.dto.BeneficiaryDto;


public interface BeneficiaryService {
	ApiResponse addbeneficiary(int accountid,  BeneficiaryDto beneficiaryDto);
	//beneficiary service
}
